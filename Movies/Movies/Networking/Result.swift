//
//  Result.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

enum Result<T, E> {
    case success(T)
    case failure(E)
}
