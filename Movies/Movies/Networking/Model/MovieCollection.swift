//
//  MovieCollection.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

struct MovieCollection: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case overview
        case parts
    }
    
    let id: Int
    let name: String?
    let overview: String?
    let parts: [MovieListItem]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        overview = try container.decodeIfPresent(String.self, forKey: .overview)
        
        do {
            parts = try container.decode([MovieListItem].self, forKey: .parts)
        } catch {
            parts = []
        }
    }
    
}
