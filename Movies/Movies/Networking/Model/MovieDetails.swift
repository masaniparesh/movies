//
//  MovieDetails.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

struct MovieDetails: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case posterPath = "poster_path"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case collection = "belongs_to_collection"
    }
    
    let id: Int
    let title: String?
    let posterPath: String?
    let voteAverage: Double?
    let voteCount: Int?
    let collection: MovieCollection?
}
