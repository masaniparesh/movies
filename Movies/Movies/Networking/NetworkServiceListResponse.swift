//
//  NetworkServiceListResponse.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

struct NetworkServiceListResponse<Model: Decodable>: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case page
        case results
    }
    
    let page: Int
    let results: [Model]
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        page = try container.decode(Int.self, forKey: .page)
        
        do {
            results = try container.decode([Model].self, forKey: .results)
        } catch {
            results = []
        }
    }
    
}
