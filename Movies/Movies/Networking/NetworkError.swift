//
//  NetworkErrorMapper.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

enum NetworkError {
    case invalidResponse
    case unauthorised
    case serverInternalError
    case invalidRequestParameter
    case unknownError
    case serviceTimedOut
    case noInternetConnection
}

extension NetworkError {
    // This can be made better by adding all custom errors

    var errorDescription: String {
        switch self {
        case .invalidResponse:
            return "Invalid Response"
        default:
            return "Unknown Error"
        }
    }
}

struct NetworkErrorMapper {
    func serviceError(from statusCode: Int?, error: Error?) -> NetworkError {
        if let statusCode = statusCode {
            return serviceError(from: statusCode)
        }
        
        if let error = error {
            return serviceError(from: error)
        }
        
        return .unknownError
    }
    
    private func serviceError(from statusCode: Int) -> NetworkError {
        switch statusCode {
        case 401:
            return .unauthorised
        case 500...599:
            return .serverInternalError
        default:
            return .unknownError
        }
    }
    
    private func serviceError(from error: Error) -> NetworkError {
        switch (error as NSError).code {
        case NSURLErrorNetworkConnectionLost:
            return .noInternetConnection
        case NSURLErrorTimedOut:
            return .serviceTimedOut
        default:
            return .unknownError
        }
    }
}
