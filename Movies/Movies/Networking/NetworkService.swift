//
//  NetworkService.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

typealias NowPlayingResponse = NetworkServiceListResponse<MovieListItem>

typealias NetworkResult<T> = Result<T, NetworkError>

protocol NowPlayingServiceProtocol {
    func getNowPlayingMovies(completion: @escaping (NetworkResult<NowPlayingResponse>) -> Void)
}

protocol DetailsServiceProtocol {
    func getMovieDetails(movieId: Int, completion: @escaping (NetworkResult<MovieDetails>) -> Void)
    func getCollectionDetails(collectionId: Int, completion: @escaping (NetworkResult<MovieCollection>) -> Void)
}

typealias NetworkServiceProtocol = NowPlayingServiceProtocol & DetailsServiceProtocol

class NetworkService: NetworkServiceProtocol {
    
    let environment = Environment()
    
    func getNowPlayingMovies(completion: @escaping (Result<NowPlayingResponse, NetworkError>) -> Void) {
        dataTask(withPath: "/movie/now_playing", completion: completion).resume()
    }
    
    func getMovieDetails(movieId: Int, completion: @escaping (NetworkResult<MovieDetails>) -> Void) {
        dataTask(withPath: "/movie/\(movieId)", completion: completion).resume()
    }
    
    func getCollectionDetails(collectionId: Int, completion: @escaping (NetworkResult<MovieCollection>) -> Void) {
        dataTask(withPath: "/collection/\(collectionId)", completion: completion).resume()
    }
    
}

private extension NetworkService {
    
    func dataTask<T: Decodable>(withPath path: String, completion: @escaping (NetworkResult<T>) -> Void) -> URLSessionDataTask {
        let url = environment.constructURL(path).require()
        
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            let statusCode = (response as? HTTPURLResponse)?.statusCode
            
            guard let data = data, statusCode == 200 else {
                let serviceError = NetworkErrorMapper().serviceError(from: statusCode, error: error)
                completion(.failure(serviceError))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let parsedResponse = try decoder.decode(T.self, from: data)
                completion(.success(parsedResponse))
            } catch {
                completion(.failure(.invalidResponse))
            }
        }
        return task
    }
    
}
