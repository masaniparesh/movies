//
//  Environment.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

struct Environment {
    let baseUrlString = "https://api.themoviedb.org/3"
    let apiKey = "ff492e9a28c2c8dacdadf2b667dbef6e"
    
    func constructURL(_ path: String) -> URL? {
        let urlString = "\(baseUrlString)\(path)?api_key=\(apiKey)"
        return URL(string: urlString)
    }
}
