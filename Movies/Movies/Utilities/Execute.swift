//
//  Execute.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

func executeOnMainThread(closure: @escaping () -> Void) {
    if Thread.isMainThread {
        closure()
    } else {
        DispatchQueue.main.async(execute: closure)
    }
}
