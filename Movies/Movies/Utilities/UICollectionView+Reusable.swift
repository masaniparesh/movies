//
//  UICollectionView+Reusable.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func registerNibCell<T: UICollectionViewCell>(nibClass: T.Type) {
        let nibName = "\(T.self)"
        let bundle = Bundle(for: nibClass)
        self.register(UINib(nibName: nibName, bundle: bundle), forCellWithReuseIdentifier: nibName)
    }
    
    func registerNibHeader<T: UICollectionReusableView>(nibClass: T.Type) {
        let nibName = "\(T.self)"
        let bundle = Bundle(for: nibClass)
        self.register(UINib(nibName: nibName, bundle: bundle),
                      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: nibName)
    }
    
    func registerNibFooter<T: UICollectionReusableView>(nibClass: T.Type) {
        let nibName = "\(T.self)"
        let bundle = Bundle(for: nibClass)
        self.register(UINib(nibName: nibName, bundle: bundle),
                      forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: nibName)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(className: T.Type, for indexPath: IndexPath) -> T {
        let name = "\(T.self)"
        guard let cell = self.dequeueReusableCell(withReuseIdentifier: name, for: indexPath) as? T else {
            fatalError("Error: cell with identifier: \(NSStringFromClass(T.self)) " +
                "for index path: \(indexPath) is not \(T.self)")
        }
        return cell
    }
    
    func dequeueReusableHeader<T: UICollectionReusableView>(className: T.Type, for indexPath: IndexPath) -> T {
        let name = "\(T.self)"
        guard let cell = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                          withReuseIdentifier: name, for: indexPath) as? T else {
                                                            fatalError("Error: dequeueReusableSupplementaryViewOfKind with identifier: \(NSStringFromClass(T.self))" +
                                                                "for index path: \(indexPath) is not \(T.self)")
        }
        return cell
    }
    
    func dequeueReusableFooter<T: UICollectionReusableView>(className: T.Type, for indexPath: IndexPath) -> T {
        let name = "\(T.self)"
        guard let cell = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter,
                                                          withReuseIdentifier: name, for: indexPath) as? T else {
                                                            fatalError("Error: dequeueReusableSupplementaryViewOfKind with identifier: \(NSStringFromClass(T.self))" +
                                                                "for index path: \(indexPath) is not \(T.self)")
        }
        return cell
    }
    
}
