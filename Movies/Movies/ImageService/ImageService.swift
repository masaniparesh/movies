//
//  ImageService.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

class ImageDownloadTask {
    
    private let dataTask: URLSessionDataTask
    
    init(dataTask: URLSessionDataTask) {
        self.dataTask = dataTask
    }
    
    func cancel() {
        dataTask.cancel()
    }
}

protocol ImageServiceProtocol {
    
    @discardableResult
    func downloadImage(withPath path: String, completion: @escaping (UIImage?) -> Void) -> ImageDownloadTask?
    
}

class ImageService: ImageServiceProtocol {

    enum Constants {
        static let baseURL = "https://image.tmdb.org/t/p/w500"
    }
    
    static let shared = ImageService()
    
    private var cache = NSCache<NSString, UIImage>()
    
    @discardableResult
    func downloadImage(withPath path: String, completion: @escaping (UIImage?) -> Void) -> ImageDownloadTask? {
        let urlString = Constants.baseURL + path
        if let cachedImage = cache.object(forKey: urlString as NSString) {
            completion(cachedImage)
            return nil
        }
        guard let url = URL(string: urlString) else {
            completion(nil)
            return nil
        }
        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error as? URLError, error.code == .cancelled {
                return
            }
            DispatchQueue.main.async {
                if let data = data, let image = UIImage(data: data) {
                    self.cache.setObject(image, forKey: urlString as NSString)
                    completion(image)
                } else {
                    completion(nil)
                }
            }
        }
        task.resume()
        return ImageDownloadTask(dataTask: task)
    }
    
}
