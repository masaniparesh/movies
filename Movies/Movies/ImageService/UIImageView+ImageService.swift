//
//  UIImageView+ImageService.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import UIKit

extension UIImageView {
    
    private static var loadTaskKey: UInt8 = 0
    
    private var loadTask: ImageDownloadTask? {
        get {
            return objc_getAssociatedObject(self, &UIImageView.loadTaskKey) as? ImageDownloadTask
        }
        set {
            objc_setAssociatedObject(self, &UIImageView.loadTaskKey, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func loadImage(atPath path: String) {
        image = nil
        cancelLoading()
        loadTask = ImageService.shared.downloadImage(withPath: path) { image in
            self.image = image
        }
    }
    
    func cancelLoading() {
        loadTask?.cancel()
    }
    
}
