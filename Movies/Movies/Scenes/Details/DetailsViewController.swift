//
//  DetailsViewController.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import UIKit

protocol DetailsViewProtocol: class {
    
    func displayDetails(_ details: DetailsMovieViewModel)
    
    func displayCollection(_ collection: DetailsCollectionViewModel)
    
    func displayError(_ error: NetworkError)
    
}

class DetailsViewController: UIViewController, StoryboardIdentifiable, DetailsViewProtocol {
    
    var presenter: DetailsPresenterProtocol!
    
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var posterImageView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var ratingLabel: UILabel!
    
    private let dataSource = DetailsTableViewDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = dataSource
        presenter.didLoadView()
    }
    
    func displayDetails(_ details: DetailsMovieViewModel) {
        if let posterPath = details.posterPath {
            posterImageView.loadImage(atPath: posterPath)
        }
        titleLabel.text = details.title
        if let rating = details.rating, let voteCount = details.voteCount {
            ratingLabel.text = String(format: "%.1f/\(voteCount)", rating)
        }
    }
    
    func displayCollection(_ collection: DetailsCollectionViewModel) {
        dataSource.collection = collection
        tableView.reloadData()
    }
    
    func displayError(_ error: NetworkError) {
        // TODO
        print(error)
    }
    
}

// -------------------------------------
// MARK: - UITableViewDelegate
// -------------------------------------
extension DetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = dataSource.movie(at: indexPath)
        presenter.selectMovie(withId: movie.id)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

// -------------------------------------
// MARK: - DetailsTableViewDataSource
// -------------------------------------
class DetailsTableViewDataSource: NSObject, UITableViewDataSource {
    
    var collection: DetailsCollectionViewModel?
    
    private let reuseIdentifier = "cell"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collection?.movies.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = collection.require().movies[indexPath.row]
        let cell: UITableViewCell
        if let dequeuedCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) {
            cell = dequeuedCell
        } else {
            cell = UITableViewCell(style: .default, reuseIdentifier: reuseIdentifier)
        }
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .white
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = movie.title
        return cell
    }
    
    func movie(at indexPath: IndexPath) -> DetailsCollectionViewModel.Movie {
        return collection.require().movies[indexPath.row]
    }
    
}
