//
//  DetailsPresenter.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

protocol DetailsPresenterProtocol: class {
    
    func didLoadView()
    
    func selectMovie(withId id: Int)
    
}

class DetailsPresenter: DetailsPresenterProtocol {
    
    weak var view: DetailsViewProtocol?
    
    let router: MovieDetailsRouterProtocol
    let movieId: Int
    let service: DetailsServiceProtocol
    
    init(router: MovieDetailsRouterProtocol, movieId: Int, service: DetailsServiceProtocol = NetworkService()) {
        self.router = router
        self.movieId = movieId
        self.service = service
    }
    
    func didLoadView() {
        loadMovieDetails()
    }
    
    func selectMovie(withId id: Int) {
        router.showMovieDetails(withId: id)
    }
    
    private func loadMovieDetails() {
        service.getMovieDetails(movieId: movieId) { [weak self] result in
            guard let self = self else {
                return
            }
            executeOnMainThread {
                switch result {
                case .success(let response):
                    let details = self.viewModel(from: response)
                    self.view?.displayDetails(details)
                    if let collection = response.collection {
                        self.loadCollection(collectionId: collection.id)
                    }
                case .failure(let networkError):
                    self.view?.displayError(networkError)
                }
            }
        }
    }
    
    private func loadCollection(collectionId: Int) {
        service.getCollectionDetails(collectionId: collectionId) { [weak self] result in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                    let collection = self.viewModel(from: response)
                    self.view?.displayCollection(collection)
                case .failure(let networkError):
                    self.view?.displayError(networkError)
                }
            }
        }
    }
    
    private func viewModel(from details: MovieDetails) -> DetailsMovieViewModel {
        return DetailsMovieViewModel(title: details.title, posterPath: details.posterPath, rating: details.voteAverage, voteCount: details.voteCount)
    }
    
    private func viewModel(from collection: MovieCollection) -> DetailsCollectionViewModel {
        let movies = collection.parts.filter { $0.id != movieId }.map {
            return DetailsCollectionViewModel.Movie(id: $0.id, title: $0.title)
        }
        return DetailsCollectionViewModel(name: collection.name, movies: movies)
    }
}
