//
//  DetailsMovieViewModel.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

struct DetailsMovieViewModel {
    
    let title: String?
    let posterPath: String?
    let rating: Double?
    let voteCount: Int?

}

struct DetailsCollectionViewModel {
    
    struct Movie {
        let id: Int
        let title: String?
    }
    
    var name: String?
    var movies: [Movie]

}
