//
//  Router.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

protocol MovieDetailsRouterProtocol: class {
    func showMovieDetails(withId: Int)
}

/// Contains navigation logic
class Router: MovieDetailsRouterProtocol {
    
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        showNowPlaying()
    }
    
    func showNowPlaying() {
        let viewController = SceneAssembly.assemblyScene(.nowPlaying(router: self))
        navigationController.pushViewController(viewController, animated: false)
    }
    
    func showMovieDetails(withId id: Int) {
        let viewController = SceneAssembly.assemblyScene(.details(movieId: id, router: self))
        navigationController.pushViewController(viewController, animated: true)
    }
    
}
