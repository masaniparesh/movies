//
//  NowPlayingCell.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import UIKit

class NowPlayingCell: UICollectionViewCell {
    
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var posterImageView: UIImageView!
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var posterImagePath: String? {
        didSet {
            if let path = posterImagePath {
                posterImageView.loadImage(atPath: path)
            } else {
                posterImageView.image = nil
            }
        }
    }
    
}
