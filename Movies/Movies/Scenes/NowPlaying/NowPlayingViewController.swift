//
//  NowPlayingViewController.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

protocol NowPlayingViewProtocol: class {
    
    func displayMovies(_ movies: [NowPlayingMovieViewModel])
    func displayError(_ error: NetworkError)
    
}

class NowPlayingViewController: UIViewController, StoryboardIdentifiable, NowPlayingViewProtocol {
    
    var presenter: NowPlayingPresenter!
    
    @IBOutlet private var collectionView: UICollectionView!
    
    private let dataSource = NowPlayingCollectionViewDataSource()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.didLoadView()
        collectionView.dataSource = dataSource
    }
    
    func displayMovies(_ movies: [NowPlayingMovieViewModel]) {
        dataSource.movies = movies
        collectionView.reloadData()
    }
    
    func displayError(_ error: NetworkError) {
        // TODO
        print(error)
    }
    
}

// -------------------------------------
// MARK: - UICollectionViewDelegate
// -------------------------------------
extension NowPlayingViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let movie = dataSource.movie(at: indexPath)
        presenter.selectMovie(withId: movie.id)
    }
    
}

// -------------------------------------
// MARK: - UICollectionViewDelegateFlowLayout
// -------------------------------------
extension NowPlayingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let columnCount: CGFloat = 2
        let spacing: CGFloat = 10
        let ratio: CGFloat = 41/27
        
        let allSpaces = (columnCount + 1) * spacing
        let allCells = collectionView.bounds.width - allSpaces
        
        let width = allCells / columnCount
        let height = width * ratio
        return CGSize(width: width, height: height)
    }
    
}

// -------------------------------------
// MARK: - NowPlayingCollectionViewDataSource
// -------------------------------------
class NowPlayingCollectionViewDataSource: NSObject, UICollectionViewDataSource {
    
    var movies: [NowPlayingMovieViewModel] = []
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let movie = movies[indexPath.item]
        let cell = collectionView.dequeueReusableCell(className: NowPlayingCell.self, for: indexPath)
        cell.title = movie.title
        cell.posterImagePath = movie.posterPath
        return cell
    }
    
    func movie(at indexPath: IndexPath) -> NowPlayingMovieViewModel {
        return movies[indexPath.item]
    }
    
}
