//
//  NowPlayingMovieViewModel.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

struct NowPlayingMovieViewModel {
    
    let id: Int
    let title: String?
    let posterPath: String?
    
}
