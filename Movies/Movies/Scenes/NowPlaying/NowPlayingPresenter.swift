//
//  NowPlayingPresenter.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation

protocol NowPlayingPresenterProtocol: class {
    
    func didLoadView()
    func selectMovie(withId id: Int)
    
}

class NowPlayingPresenter: NowPlayingPresenterProtocol {
    
    weak var view: NowPlayingViewProtocol?
    
    let router: MovieDetailsRouterProtocol
    let service: NowPlayingServiceProtocol
    
    init(router: MovieDetailsRouterProtocol, service: NowPlayingServiceProtocol = NetworkService()) {
        self.router = router
        self.service = service
    }
    
    func didLoadView() {
        service.getNowPlayingMovies { [weak self] result in
            guard let self = self else {
                return
            }
            executeOnMainThread {
                switch result {
                case .success(let moviesResponse):
                    let movies = moviesResponse.results.map(self.movieViewModel)
                    self.view?.displayMovies(movies)
                case .failure(let networkError):
                    self.view?.displayError(networkError)
                }
            }
        }
    }
    
    func selectMovie(withId id: Int) {
        router.showMovieDetails(withId: id)
    }
    
    private func movieViewModel(from movie: MovieListItem) -> NowPlayingMovieViewModel {
        return NowPlayingMovieViewModel(id: movie.id, title: movie.title, posterPath: movie.posterPath)
    }
    
}
