//
//  SceneAssembly.swift
//  Movies
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import Foundation
import UIKit

struct SceneAssembly {
    
    enum Scene {
        case nowPlaying(router: MovieDetailsRouterProtocol)
        case details(movieId: Int, router: MovieDetailsRouterProtocol)
    }
    
    static func assemblyScene(_ scene: Scene) -> UIViewController {
        switch scene {
        case .nowPlaying(let router):
            let viewController: NowPlayingViewController = UIStoryboard(storyboard: .nowPlaying).instantiateViewController()
            let presenter = NowPlayingPresenter(router: router)
            viewController.presenter = presenter
            presenter.view = viewController
            return viewController
        case .details(let movieId, let router):
            let viewController: DetailsViewController = UIStoryboard(storyboard: .details).instantiateViewController()
            let presenter = DetailsPresenter(router: router, movieId: movieId)
            viewController.presenter = presenter
            presenter.view = viewController
            return viewController
        }
    }
    
}
