//
//  NowPlayingPresenterTests.swift
//  MoviesTests
//
//  Created by Paresh Masani on 06/11/2018.
//  Copyright © 2018 Paresh Masani. All rights reserved.
//

import XCTest
@testable import Movies

class NowPlayingPresenterTests: XCTestCase {
    
    private lazy var moviesFromJson: NowPlayingResponse = {
        let resourceURL = Bundle(for: NowPlayingPresenterTests.self).url(forResource: "nowPlaying", withExtension: "json")!
        let data = try! Data(contentsOf: resourceURL)
        return try! JSONDecoder().decode(NowPlayingResponse.self, from: data)
    }()
    
    func testLoadSuccess() {
        let view = NowPlayingViewMock()
        let service = NetworkServiceMock()
        service.result = .success(moviesFromJson)
        let router = Router(navigationController: UINavigationController())
        let presenter = NowPlayingPresenter(router: router, service: service)
        presenter.view = view
        
        presenter.didLoadView()
        
        XCTAssertTrue(view.movies.count == 2)
        XCTAssertTrue(view.movies[0].id == 335983)
        XCTAssertTrue(view.movies[0].title == "Venom")
        XCTAssertTrue(view.movies[0].posterPath == "/2uNW4WbgBXL25BAbXGLnLqX71Sw.jpg")
        
        XCTAssertTrue(view.movies[1].id == 260513)
        XCTAssertTrue(view.movies[1].title == "Incredibles 2")
        XCTAssertTrue(view.movies[1].posterPath == "/x1txcDXkcM65gl7w20PwYSxAYah.jpg")
        
        XCTAssertNil(view.error)
    }
    
    func testLoadFailure() {
        let view = NowPlayingViewMock()
        let service = NetworkServiceMock()
        service.result = .failure(.invalidResponse)
        let router = Router(navigationController: UINavigationController())
        let presenter = NowPlayingPresenter(router: router, service: service)
        presenter.view = view
        
        presenter.didLoadView()
        
        XCTAssertTrue(view.movies.isEmpty)
        XCTAssertTrue(view.error == .invalidResponse)
    }
    
}

// -------------------------------------
// MARK: - Mock Classes
// -------------------------------------

class NowPlayingViewMock: NowPlayingViewProtocol {
    
    private(set) var error: NetworkError?
    private(set) var movies: [NowPlayingMovieViewModel] = []
    
    func displayError(_ error: NetworkError) {
        self.error = error
    }
    
    func displayMovies(_ movies: [NowPlayingMovieViewModel]) {
        self.movies = movies
    }
    
}

class NetworkServiceMock: NowPlayingServiceProtocol {
    
    var result: Result<NetworkServiceListResponse<MovieListItem>, NetworkError> = .failure(.unknownError)
    
    func getNowPlayingMovies(completion: @escaping (Result<NetworkServiceListResponse<MovieListItem>, NetworkError>) -> Void) {
        completion(result)
    }
    
}
